﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class LargestNumber179Tests
    {
        [Test]
        [TestCase(new int[] { 10, 2 }, "210")]
        [TestCase(new int[] { 3, 30, 34, 5, 9 }, "9534330")]
        [TestCase(new int[] { 432, 43243 }, "43243432")]
        public void LargestNumberTest(int[] nums, string expected)
        {
           var actual = new LargestNumber179().LargestNumber(nums);
           Assert.AreEqual(expected, actual);
        }
    }
}
