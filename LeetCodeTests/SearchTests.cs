﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class SearchTests
    {
        private readonly int[] a = new int[] { 3, 4, 6, 9, 10, 12, 14, 15, 17, 19, 21 };

        [Test]
        [TestCase(0, 3)]
        [TestCase(2, 3)]
        [TestCase(3, 3)]
        [TestCase(21, 21)]
        [TestCase(25, -1)]
        [TestCase(20, 21)]
        [TestCase(10, 10)]
        [TestCase(13, 14)]
        public void BinarySearchTest(int searchValue, int expected)
        {
            var result = Search.BinarySearch(a, searchValue);
            Assert.AreEqual(expected, result);
        }
    }
}
