﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class ExpressiveWords809Tests
    {
        [Test]
        [TestCase(new string[] { "hello", "hi", "helo" }, "heeellooo", 1)]
        public void ExpressiveWordsTest(string[] words, string s, int expected)
        {
            var result = new ExpressiveWords809().ExpressiveWords(s, words);
            Assert.AreEqual(expected, result);
        }
    }
}