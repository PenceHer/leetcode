﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class StringTests
    {
        [Test]
        [TestCase("ab", "eidbaooo", true)]
        [TestCase("ab", "eidboaoo", false)]
        public void PermutationInStringTest(string s1, string s2, bool expected)
        {
            bool actual = new PermutationInString().CheckInclusion(s1, s2);
            Assert.AreEqual(expected, actual);
        }
    }
}