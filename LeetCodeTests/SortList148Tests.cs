﻿using LeetCode;
using NUnit.Framework;
using System.Collections.Generic;

namespace LeetCodeTests
{
    [TestFixture]
    public class SortList148Tests
    {
        [Test]
        [TestCase(new int[] { 4, 2, 1, 3 }, new int[] { 1, 2, 3, 4 })]
        [TestCase(new int[] { -1, 5, 3, 4, 0 }, new int[] { -1, 0, 3, 4, 5 })]
        [TestCase(new int[0], new int[0])]
        public void SortListTest(int[] input, int[] expected)
        {
            ListNode head = null;

            for (int i = input.Length - 1; i >= 0; i--)
                head = new ListNode(input[i], head);

            var result = new SortList148().SortList(head);
            var resultList = NodeToList(result);

            Assert.AreEqual(expected, resultList);
        }

        [Test]
        [TestCase(new int[] { 4, 2, 1, 3 }, new int[] { 1, 2, 3, 4 })]
        [TestCase(new int[] { -1, 5, 3, 4, 0 }, new int[] { -1, 0, 3, 4, 5 })]
        [TestCase(new int[0], new int[0])]
        [TestCase(new int[] { 1, 2, 3, 4 }, new int[] { 1, 2, 3, 4 })]
        public void SortListInPlaceTest(int[] input, int[] expected)
        {
            ListNode head = null;

            for (int i = input.Length - 1; i >= 0; i--)
                head = new ListNode(input[i], head);

            var result = new SortList148().SortListInPlace(head);
            var resultList = NodeToList(result);

            Assert.AreEqual(expected, resultList);
        }

        private static List<int> NodeToList(ListNode head)
        {
            var nodeList = new List<int>();
            while (head != null)
            {
                nodeList.Add(head.val);
                head = head.next;
            }
            return nodeList;
        }
    }
}