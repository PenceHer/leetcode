﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class MaximumProductOfWordLengthsTests
    {
        [Test]
        [TestCase(new string[] { "abcw", "baz", "foo", "bar", "xtfn", "abcdef" }, 16)]
        public void MaxProductTest(string[] words, int expected)
        {
            int result = new MaximumProductOfWordLengths().MaxProduct(words);
            Assert.AreEqual(expected, result);
        }
    }
}