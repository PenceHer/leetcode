﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class OddEvenLinkedList328Tests
    {
        [Test]
        [TestCase(new int[] { 1, 2, 3, 4, 5 }, new int[] { 1, 3, 5, 2, 4 } )]
        [TestCase(new int[] { 2, 1, 3, 5, 6, 4, 7 }, new int[] { 2, 3, 6, 7, 1, 5, 4 })]
        [TestCase(new int[0], new int[0])]
        [TestCase(new int[] { 1 }, new int[] { 1 })]
        public void OddEvenListTest(int[] input, int[] expected)
        {
            var head = ConvertToLinkedList(input);
            var actual = new OddEvenLinkedList328().OddEvenList(head);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual.val);
                actual = actual.next;
            }
            Assert.AreEqual(null, actual);
        }

        private ListNode ConvertToLinkedList(int[] input)
        {
            ListNode head = null;

            for (int i = input.Length - 1; i >= 0; i--)
            {
                head = new ListNode(input[i], head);
            }

            return head;
        }
    }
}
