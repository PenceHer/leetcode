﻿using LeetCode;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeTests
{
    [TestFixture]
    public class SortColors75Tests
    {
        [Test]
        [TestCase(new int[] { 2, 0 ,2 ,1 ,1 ,0 }, new int[] { 0, 0, 1, 1, 2, 2 })]
        [TestCase(new int[] { 2, 0, 1 }, new int[] { 0, 1, 2 })]
        public void SortColorsTest(int[] nums, int[] expected)
        {
            new SortColors75().SortColors(nums);
            Assert.AreEqual(expected, nums);
        }
    }
}
