﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class MedianOfTwoSortedArrays4Tests
    {
        [Test]
        [TestCase(new int[] { 1, 3 }, new int[] { 2 }, 2)]
        [TestCase(new int[] { 1, 2 }, new int[] { 3, 4 }, 2.5)]
        [TestCase(new int[] { 0, 0 }, new int[] { 0, 0 }, 0)]
        public void FindMedianSortedArrays(int[] nums1, int[] nums2, double expected)
        {
            var actual = new MedianOfTwoSortedArrays4().FindMedianSortedArrays(nums1, nums2);
            Assert.AreEqual(expected, actual);
        }
    }
}
