﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class MergeIntervals56Tests
    {
        [Test]
        [TestCaseSource(nameof(MergeTestCases))]
        public void MergeTest(int[][] intervals, int[][] expected)
        {
            var actual = new MergeIntervals56().Merge(intervals);
            Assert.AreEqual(expected, actual);
        }

        static readonly object[] MergeTestCases =
        {
            new object[] { new int[][] { new int[] { 1, 3 }, new int[] { 2, 6 }, new int[] { 8, 10 }, new int[] { 15, 18 } },
                new int[][] { new int[] { 1, 6 }, new int[] { 8, 10 }, new int[] { 15, 18 } }},
            new object[] { new int[][] { new int[] { 1, 4 }, new int[] { 0, 4 } },
                new int[][] { new int[] { 0, 4 } } },
            new object[] { new int[][] { new int[] { 1, 4 }, new int[] { 2, 3 } },
                new int[][] { new int[] { 1, 4 } } },
        };
    }
}