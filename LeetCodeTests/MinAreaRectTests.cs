﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class MinAreaRectTests
    {
        [Test]
        [TestCaseSource(nameof(MinAreaRectTestSource))]
        public void MinAreaRectTest(int[][] points, int expected)
        {
            int actual = new MinimumAreaRectangle().MinAreaRect(points);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        [TestCaseSource(nameof(MinAreaRectTestSource))]
        public void MinAreaRectSimpleTest(int[][] points, int expected)
        {
            int actual = new MinimumAreaRectangle().MinAreaRectSimple(points);
            Assert.AreEqual(expected, actual);
        }

        static readonly object[] MinAreaRectTestSource =
        {
            //new object[] 
            //{ 
            //    new int[][]
            //    {
            //        new int[] { 1, 1 },
            //        new int[] { 1, 3 },
            //        new int[] { 3, 1 },
            //        new int[] { 3, 3 },
            //        new int[] { 4, 1 },
            //        new int[] { 4, 3 },
            //    },
            //    2
            //},
            //new object[]
            //{
            //    new int[][]
            //    {
            //        new int[] { 1, 1 },
            //        new int[] { 1, 3 },
            //        new int[] { 3, 1 },
            //        new int[] { 3, 3 },
            //        new int[] { 2, 2 },
            //    },
            //    4
            //},
            new object[]
            {
                new int[][]
                {
                    new int[] { 0, 1 },
                    new int[] { 1, 3 },
                    new int[] { 3, 3 },
                    new int[] { 4, 4 },
                    new int[] { 1, 4 },
                    new int[] { 2, 3 },
                    new int[] { 1, 0 },
                    new int[] { 3, 4 },
                },
                2
            },
        };
    }
}
