﻿using LeetCode;
using NUnit.Framework;

namespace LeetCodeTests
{
    [TestFixture]
    public class ImplementStackUsingQueuesTests
    {
        [Test]
        public void MyStackTest()
        {
            var stack = new MyStack();
            stack.Push(1);
            stack.Push(2);
            Assert.AreEqual(2, stack.Top());
            Assert.AreEqual(2, stack.Pop());
            Assert.AreEqual(false, stack.Empty());
        }
    }
}
