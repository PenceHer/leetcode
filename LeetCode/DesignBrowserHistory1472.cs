﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    public class BrowserHistory
    {
        private readonly List<string> History;
        private int index;
        private int maxIndex;

        private string Current => History[index];

        public BrowserHistory(string homepage)
        {
            History = new List<string>();
            index = -1;
            maxIndex = 0;
            Visit(homepage);
        }

        public void Visit(string url)
        {
            if (History.Count == ++index)
                History.Add(url);
            else
                History[index] = url;

            maxIndex = index;
        }

        public string Back(int steps)
        {
            index = Math.Max(index - steps, 0);
            return Current;
        }

        public string Forward(int steps)
        {
            index = Math.Min(index + steps, maxIndex);
            return Current;
        }
    }
}
