﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    public class Search
    {
        public static int BinarySearch(int[] input, int searchValue)
        {
            if (input == null ||
                input.Length == 0 ||
                input[^1] < searchValue)
            {
                return -1;
            }

            if (input[0] > searchValue)
                return input[0];

            int low = 0;
            int high = input.Length - 1;

            while (low < high)
            {
                int mid = (low + high) / 2;
                int midValue = input[mid];

                if (midValue == searchValue)
                    return searchValue;

                if (midValue > searchValue)
                    high = mid;
                else
                    low = mid + 1;
            }

            return input[high];
        }
    }
}
