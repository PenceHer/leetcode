﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode
{
    public class MergeIntervals56
    {
        public int[][] Merge(int[][] intervals)
        {
            var mergedIntervals = new List<int[]>();
            var sortedIntervals = intervals.OrderBy(interval => interval[0]);
            int[] currentInterval = sortedIntervals.First();

            foreach (var interval in sortedIntervals.Skip(1))
            {
                if (currentInterval[1] >= interval[0])
                    currentInterval[1] = Math.Max(interval[1], currentInterval[1]);
                else
                {
                    mergedIntervals.Add(currentInterval);
                    currentInterval = interval;
                }
            }
            mergedIntervals.Add(currentInterval);

            return mergedIntervals.ToArray();
        }
    }
}
