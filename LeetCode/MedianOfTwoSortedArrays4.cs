﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    public class MedianOfTwoSortedArrays4
    {
        public double FindMedianSortedArrays(int[] nums1, int[] nums2)
        {
            int i = 0, j = 0;
            int size = nums1.Length + nums2.Length;
            bool isEven = size % 2 == 0;
            int midPoint = size / 2 - (isEven ? 1 : 0);

            for (int x = 0; x < midPoint; x++)
                Next();

            double median = Next();
            if (isEven)
                median = (median + Next()) / 2.0;

            return median;


            int Next()
            {
                if (j >= nums2.Length)
                    return nums1[i++];
                else if (i >= nums1.Length)
                    return nums2[j++];
                else if (nums1[i] < nums2[j])
                    return nums1[i++];
                else
                    return nums2[j++];
            }
        }
    }
}
