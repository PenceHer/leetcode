﻿using System.Collections.Generic;
using System.Linq;

namespace LeetCode
{
    public class SortList148
    {
        public ListNode SortList(ListNode head)
        {
            var nodes = new List<ListNode>();

            while (head != null)
            {
                nodes.Add(head);
                head = head.next;
            }

            nodes = nodes.OrderBy(node => node.val).ToList();

            for (int i = 1; i < nodes.Count; i++)
            {
                nodes[i - 1].next = nodes[i];
            }
            if (nodes.Any())
            {
                nodes.Last().next = null;
                return nodes.First();
            }

            return null;
        }

        public ListNode SortListInPlace(ListNode head)
        {
            int length = 0;
            ListNode current = head;

            while (current != null)
            {
                current = current.next;
                length++;
            }

            return SortListInPlace(head, length);
        }

        private ListNode SortListInPlace(ListNode head, int length)
        {
            if (length < 2) return head;
            ListNode midPoint = head;

            int leftSize = length / 2;
            for (int i = 0; i < leftSize - 1; i++)
                midPoint = midPoint.next;

            var lastLeft = midPoint;
            midPoint = lastLeft.next;
            lastLeft.next = null;

            var left = SortListInPlace(head, leftSize);
            var right = SortListInPlace(midPoint, length - leftSize);
            return Merge(left, right);
        }

        private static ListNode Merge(ListNode left, ListNode right)
        {
            ListNode head = MinNode();
            ListNode current = head;

            while (left != null && right != null)
            {
                current.next = MinNode();
                current = current.next;
            }
            current.next = left ?? right;

            return head;


            ListNode MinNode()
            {
                ListNode min;

                if (left == null || left.val > right.val)
                {
                    min = right;
                    right = right.next;
                }
                else
                {
                    min = left;
                    left = left.next;
                }

                return min;
            }
        }
    }

    public class ListNode
    {
        public int val;
        public ListNode next;

        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
}
