﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LeetCode
{
    public class LargestNumber179
    {
        public string LargestNumber(int[] nums)
        {
            var numbers = nums.Select(num => num.ToString());
            var bigNumber = numbers.OrderByDescending(num => num, new StringNumberComparer());
            string bigString = string.Join("", bigNumber).TrimStart('0');

            return bigString == "" ? "0" : bigString;
        }

        private class StringNumberComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                int minLength = Math.Min(x.Length, y.Length);
                for (int i = 0; i < minLength; i++)
                {
                    if (x[i] > y[i])
                        return 1;
                    if (x[i] < y[i])
                        return -1;
                }

                return (x + y).CompareTo(y + x);
            }
        }
    }
}
