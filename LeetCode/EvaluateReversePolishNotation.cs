﻿using System;
using System.Collections.Generic;

namespace LeetCode
{
    public class EvaluateReversePolishNotation
    {
        public int EvalRPN(string[] tokens)
        {
            var numbers = new Stack<int>();
            var operators = new HashSet<string>() { "+", "-", "*", "/" };

            foreach (string token in tokens)
            {
                if (operators.Contains(token))
                {
                    int operandB = numbers.Pop();
                    int operandA = numbers.Pop();
                    int result = ApplyOperation(operandA, operandB, token);
                    numbers.Push(result);
                }
                else
                    numbers.Push(int.Parse(token));
            }

            return numbers.Pop();
        }

        private static int ApplyOperation(int a, int b, string op)
        {
            return op switch
            {
                "+" => a + b,
                "-" => a - b,
                "*" => a * b,
                "/" => a / b,
                _ => throw new InvalidOperationException(),
            };
        }
    }
}
