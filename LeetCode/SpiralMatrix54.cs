﻿using System.Collections.Generic;

namespace LeetCode
{
    public class SpiralMatrix54
    {
        public IList<int> SpiralOrder(int[][] matrix)
        {
            var spiral = new List<int>(matrix.Length * matrix[0].Length);
            int left = 0, right = matrix[0].Length - 1;
            int top = 0, bottom = matrix.Length - 1;
            var direction = Direction.Right;

            while (right >= left && bottom >= top)
            {
                switch (direction)
                {
                    case Direction.Right:
                        for (int x = left; x <= right; x++)
                        {
                            spiral.Add(matrix[top][x]);
                        }
                        top++;
                        direction = Direction.Down;
                        break;
                    case Direction.Down:
                        for (int y = top; y <= bottom; y++)
                        {
                            spiral.Add(matrix[y][right]);
                        }
                        right--;
                        direction = Direction.Left;
                        break;
                    case Direction.Left:
                        for (int x = right; x >= left; x--)
                        {
                            spiral.Add(matrix[bottom][x]);
                        }
                        bottom--;
                        direction = Direction.Up;
                        break;
                    case Direction.Up:
                        for (int y = bottom; y >= top; y--)
                        {
                            spiral.Add(matrix[y][left]);
                        }
                        left++;
                        direction = Direction.Right;
                        break;
                }
            }

            return spiral;
        }

        private enum Direction
        {
            Right,
            Down,
            Left,
            Up,
        }
    }
}
