﻿namespace LeetCode
{
    public class SortColors75
    {
        public void SortColors(int[] nums)
        {
            int red = 0, white = 0, blue = 0;

            foreach (int num in nums)
            {
                switch (num)
                {
                    case 0:
                        red++;
                        break;
                    case 1:
                        white++;
                        break;
                    case 2:
                        blue++;
                        break;
                }
            }

            Fill(nums, 0, red, 0);
            Fill(nums, red, white, 1);
            Fill(nums, red + white, blue, 2);
        }

        private static void Fill(int[] nums, int start, int length, int value)
        {
            for (int i = start; i < start + length; i++)
            {
                nums[i] = value;
            }
        }
    }
}
