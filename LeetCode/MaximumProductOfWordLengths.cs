﻿using System.Collections.Generic;
using System.Linq;

namespace LeetCode
{
    public class MaximumProductOfWordLengths
    {
        public int MaxProduct(string[] words)
        {
            int max = 0;
            var orderedWords = words
                .Distinct()
                .OrderByDescending(word => word.Length).ToList();
            var bitMasks = new int?[words.Length];

            for (int wordA = 0, end = orderedWords.Count - 1; wordA < end; wordA++)
            {
                for (int check = wordA + 1; check < orderedWords.Count; check++)
                {
                    int checkLength = orderedWords[wordA].Length * orderedWords[check].Length;
                    if (checkLength <= max)
                    {
                        end = check - 1;
                        break;
                    }

                    int maskA = GetBitMask(bitMasks, wordA, orderedWords[wordA]);
                    int checkMask = GetBitMask(bitMasks, check, orderedWords[check]);
                    if ((maskA & checkMask) == 0)
                    {
                        end = check - 1;
                        max = checkLength;
                        break;
                    }
                }
            }

            return max;
        }

        private static int GetBitMask(int?[] bitMasks, int index, string word)
        {
            if (bitMasks[index] != null) return (int)bitMasks[index];

            int mask = BitMask(word);
            bitMasks[index] = mask;

            return mask;
        }

        private static int BitMask(string word)
        {
            var letters = new HashSet<char>(word);
            int mask = 0;

            for (char i = 'a'; i <= 'z'; i++)
            {
                int bit = letters.Contains(i) ? 1 : 0;
                mask = (mask << 1) + bit;
            }

            return mask;
        }
    }
}
