﻿using System.Collections.Generic;
using System.Linq;

namespace LeetCode
{
    public class MyStack
    {
        private Queue<int> Queue;

        public MyStack()
        {
            Queue = new Queue<int>();
        }

        public void Push(int x)
        {
            Queue.Enqueue(x);
        }

        public int Pop()
        {
            var popQueue = new Queue<int>();
            while (Queue.Count > 1)
                popQueue.Enqueue(Queue.Dequeue());

            int pop = Queue.Peek();
            Queue = popQueue;

            return pop;
        }

        public int Top()
        {
            int top = Pop();
            Queue.Enqueue(top);
            return top;
        }

        public bool Empty()
        {
            return !Queue.Any();
        }
    }
}
