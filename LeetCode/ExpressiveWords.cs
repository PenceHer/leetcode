﻿using System.Collections.Generic;
using System.Linq;

namespace LeetCode
{
    public class ExpressiveWords809
    {
        public int ExpressiveWords(string s, string[] words)
        {
            int expressiveWords = 0;
            var compressedS = CompressWord(s);
            
            foreach (string word in words)
            {
                var compressedWord = CompressWord(word);
                if (compressedS.Count != compressedWord.Count) continue;
                bool stretchable = true;

                for (int i = 0; i < compressedS.Count; i++)
                {
                    if (!LetterIsStretchable(compressedS[i], compressedWord[i]))
                    {
                        stretchable = false;
                        break;
                    }
                }

                if (stretchable) expressiveWords++;
            }

            return expressiveWords;


            bool LetterIsStretchable(LetterRun s, LetterRun word)
            {
                if (s.Letter != word.Letter) return false;
                if (s.Run < word.Run) return false;
                if (s.Run == word.Run) return true;
                return s.Run >= 3;
            }
        }

        private static List<LetterRun> CompressWord(string word)
        {
            var compressedWord = new List<LetterRun>(word.Length) { new(word.First(), 1 ) };

            foreach (char c in word.Skip(1))
            {
                if (c == compressedWord.Last().Letter)
                    compressedWord.Last().Run++;
                else
                    compressedWord.Add(new(c, 1));
            }

            return compressedWord;
        }

        private class LetterRun
        {
            public char Letter;
            public int Run;

            public LetterRun(char letter, int run)
            {
                Letter = letter;
                Run = run;
            }
        }
    }
}
