﻿namespace LeetCode
{
    public class OddEvenLinkedList328
    {
        public ListNode OddEvenList(ListNode head)
        {
            if (head == null) return null;

            ListNode oddCurrent = head, oddHead = head;
            ListNode evenCurrent = head?.next, evenHead = head?.next;
            head = head?.next?.next;
            int counter = 0;

            while (head != null)
            {
                if (++counter % 2 == 0)
                {
                    evenCurrent.next = head;
                    evenCurrent = evenCurrent.next;
                }
                else
                {
                    oddCurrent.next = head;
                    oddCurrent = oddCurrent.next;
                }

                head = head.next;
            }

            oddCurrent.next = evenHead;
            if (evenCurrent != null)
                evenCurrent.next = null;

            return oddHead;
        }
    }
}
