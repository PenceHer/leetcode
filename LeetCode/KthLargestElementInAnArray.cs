﻿using System.Linq;

namespace LeetCode
{
    public class KthLargestElementInAnArray
    {
        public int FindKthLargest(int[] nums, int k)
        {
            return nums
                .OrderByDescending(num => num)
                .Skip(k - 1)
                .Take(1)
                .First();
        }
    }
}
