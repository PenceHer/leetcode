﻿using System.Collections.Generic;

namespace LeetCode
{
    public class LRUCache
    {
        private int Capacity;
        private Dictionary<int, Page> Cache;
        private Page LRU;
        private Page MRU;

        public LRUCache(int capacity)
        {
            Capacity = capacity;
            Cache = new();
        }

        public int Get(int key)
        {
            return GetPage(key)?.Value ?? -1;
        }

        private Page GetPage(int key)
        {
            if (Cache.ContainsKey(key))
            {
                var page = Cache[key];
                bool isLru = page.Prev == null;
                bool isMru = page.Next == null;

                if (!isMru)
                {
                    if (isLru)
                    {
                        LRU = page.Next;
                        LRU.Prev = null;
                    }
                    else
                    {
                        page.Prev.Next = page.Next;
                        page.Next.Prev = page.Prev;
                    }

                    MRU.Next = page;
                    page.Prev = MRU;
                    MRU = page;
                    page.Next = null;
                }

                return page;
            }
            else
                return null;
        }

        public void Put(int key, int value)
        {
            var page = GetPage(key);

            if (page == null)
            {
                page = new Page { Key = key, Value = value };
                Cache.Add(key, page);

                if (LRU == null)
                {
                    LRU = page;
                    MRU = page;
                }
                else
                {
                    MRU.Next = page;
                    page.Prev = MRU;
                    MRU = page;
                }

                if (Cache.Count > Capacity)
                {
                    Cache.Remove(LRU.Key);
                    LRU = LRU.Next;
                    LRU.Prev = null;
                }
            }
            else
                page.Value = value;
        }

        private class Page
        {
            public int Key;
            public int Value;
            public Page Prev;
            public Page Next;
        }
    }
}
