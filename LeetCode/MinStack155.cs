﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCode
{
    public class MinStack
    {
        private Stack<StackLevel> Stack;

        public MinStack()
        {
            Stack = new Stack<StackLevel>();
        }

        public void Push(int val)
        {
            int min;
            if (!Stack.Any())
                min = val;
            else
                min = Math.Min(val, Stack.Peek().Min);

            Stack.Push(new StackLevel() { Value = val, Min = min });
        }

        public void Pop()
        {
            Stack.Pop();
        }

        public int Top()
        {
            return Stack.Peek().Value;
        }

        public int GetMin()
        {
            return Stack.Peek().Min;
        }

        private class StackLevel
        {
            public int Value;
            public int Min;
        }
    }
}
