﻿using System.Collections.Generic;
using System.Linq;

namespace LeetCode
{
    public class PermutationInString
    {
        public bool CheckInclusion(string s1, string s2)
        {
            int length = s2.Length;
            if (s1.Length > length) return false;

            var excessLetterCounter = CreateLetterCounter(s1);
            for (int i = 0; i < s1.Length; i++)
                excessLetterCounter[s2[i]]--;

            int excessLetters = Alphabet().Count(letter => excessLetterCounter[letter] != 0);
            if (excessLetters == 0)
                return true;

            for (int i = s1.Length; i < s2.Length; i++)
            {
                char removeLetter = s2[i - s1.Length];
                char addLetter = s2[i];

                if (removeLetter != addLetter)
                {
                    if (excessLetterCounter[removeLetter] == 0)
                        excessLetters++;
                    if (++excessLetterCounter[removeLetter] == 0)
                        excessLetters--;

                    if (excessLetterCounter[addLetter] == 0)
                        excessLetters++;
                    if (--excessLetterCounter[addLetter] == 0)
                        excessLetters--;

                    if (excessLetters == 0)
                        return true;
                }
            }

            return false;
        }

        private static Dictionary<char, int> CreateLetterCounter(string s)
        {
            var letterCounter = new Dictionary<char, int>();

            foreach (char letter in Alphabet())
                letterCounter.Add(letter, 0);

            foreach (char letter in s)
                letterCounter[letter]++;

            return letterCounter;
        }

        private static List<char> Alphabet()
        {
            var alphabet = new List<char>(26);

            for (int letter = 'a'; letter <= 'z'; letter++)
                alphabet.Add((char)letter);

            return alphabet;
        }
    }
}
