﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LeetCode
{
    public class MinimumAreaRectangle
    {
        public int MinAreaRectSimple(int[][] points)
        {
            int smallestRect = int.MaxValue;
            var map = points
                .Select(x => new Point(x[0], x[1]))
                .ToHashSet();

            for (int i = 0; i < points.Length - 1; i++)
            {
                for (int j = 0; j < points.Length; j++)
                {
                    int[] a = points[i];
                    int[] b = points[j];
                    int width = Math.Abs(a[0] - b[0]);
                    int height = Math.Abs(a[1] - b[1]);
                    int area = width * height;

                    if (area > 0 &&
                        area < smallestRect &&
                        map.Contains(new Point(a[0], b[1])) &&
                        map.Contains(new Point(b[0], a[1])))
                    {
                        smallestRect = area;
                    }
                }
            }

            return smallestRect == int.MaxValue ? 0 : smallestRect;
        }

        public int MinAreaRect(int[][] points)
        {
            var columns = Linearize(points, 0);
            var rows = Linearize(points, 1);

            var orthogonalPoints = columns
                .SelectMany(x => x.Value.ToList())
                .Where(x => rows.ContainsKey(x[1]));

            var sortedColumns = LinearizeColumns(orthogonalPoints);
            int smallest = int.MaxValue;

            for (int i = 0; i < sortedColumns.Count - 1; i++)
            {
                for (int j = i + 1; j < sortedColumns.Count; j++)
                {
                    int smallestMatch = SmallestRectangle(sortedColumns[i], sortedColumns[j]);
                    if (smallestMatch > 0)
                        smallest = Math.Min(smallest, smallestMatch);
                }
            }

            return smallest == int.MaxValue ? 0 : smallest;
        }

        private static Dictionary<int, List<int[]>> Linearize(IEnumerable<int[]> points, int dimension)
        {
            return points
                .GroupBy(x => x[dimension])
                .Where(x => x.Count() > 1)
                .ToDictionary(x => x.Key, x => x.ToList());
        }

        private static List<List<int[]>> LinearizeColumns(IEnumerable<int[]> points)
        {
            return points
                .GroupBy(x => x[0])
                .OrderBy(x => x.Key)
                .Select(x => x.OrderBy(x => x[1]).ToList())
                .ToList();
        }

        private static int SmallestRectangle(IEnumerable<int[]> columnA, IEnumerable<int[]> columnB)
        {
            var left = columnA.Select(x => x[1]);
            var right = columnB.Select(x => x[1]);
            var horizontalMatches = left
                .Where(x => right.Contains(x))
                .OrderBy(x => x)
                .ToList();

            if (horizontalMatches.Count < 2)
                return 0;

            int smallestVerticalGap = horizontalMatches[1] - horizontalMatches[0];

            for (int i = 1; i < horizontalMatches.Count - 1; i++)
            {
                int nextGap = horizontalMatches[i + 1] - horizontalMatches[i];
                smallestVerticalGap = Math.Min(smallestVerticalGap, nextGap);
            }

            int horizontalGap = columnB.First()[0] - columnA.First()[0];
            return smallestVerticalGap * horizontalGap;
        }
    }
}
